//
//  ContentView.swift
//  RockPaperScissors
//
//  Created by Kashish Jain on 12/07/22.
//

import SwiftUI

struct EmojiView: View {
    var emoji: String
    var action: () -> Void
    var body: some View {
        Button(emoji, action: action)
            .font(.custom("Arial", size: 80))
            .padding()
            .background(.ultraThinMaterial)
            .clipShape(RoundedRectangle(cornerRadius: 15))
            .shadow(radius: 5)
    }
}

struct GradientBg: ViewModifier {
    func body(content: Content) -> some View {
        ZStack{
            RadialGradient(stops: [
                .init(color: Color(red: 0.76, green: 0.15, blue: 0.26), location: 0.3),
                .init(color: Color(red: 0.1, green: 0.2, blue: 0.45), location: 0.3)
            ], center: .top, startRadius: 200, endRadius: 700)
            .ignoresSafeArea()
            
            content
        }
    }
}

extension View {
    func gradientBg() -> some View {
        modifier(GradientBg())
    }
}

struct ContentView: View {
     var emojis = ["👊", "🖐", "✌️"]
    @State private var games = 0
    @State private var score = 0
    @State private var alertTitle = ""
    @State private var alertMsg = ""
    @State private var showAlert = false
    @State private var showEnd = false
    @State private var randomChoice = Int.random(in: 0..<3)
    var wins = [2, 0, 1]
    var looses = [1, 2, 0]
    
    var body: some View {
        NavigationView{
            VStack{
                Spacer()
                HStack(spacing: 20){
                    EmojiView(emoji: "👊"){ tapHandler(0) }
                    EmojiView(emoji: "🖐"){ tapHandler(1) }
                    EmojiView(emoji: "✌️"){ tapHandler(2) }
                }
                Spacer()
                Text("Score: \(score)")
                    .font(.largeTitle)
                    .foregroundColor(.white)
                    .bold()
                Spacer()
                Spacer()
                Spacer()
            }
            .gradientBg()
            .navigationTitle("Rock Paper Scissors")
            .alert(alertTitle, isPresented: $showAlert){
                Button("Continue", action: reset)
            } message: {
                Text("""
                \(alertMsg)
                Trials Pending: \(9 - games)/10
                """)
            }
            .alert("Game Over", isPresented: $showEnd){
                Button("Restart", action: restart)
            } message: {
                Text("Total Wins: \(score)/10")
            }
        }
    }
    func tapHandler(_ choice: Int){
        alertMsg = "\(emojis[choice]) X \(emojis[randomChoice])"
        if(wins[choice] == randomChoice){
            score += 1
            alertTitle = "Win !!!"
        }else if(looses[choice] == randomChoice){
            alertTitle = "Loose !!!"
        }else{
            alertTitle = "Tied"
        }
        if(games == 9){
            showEnd = true
        }else{
            showAlert = true
        }
    }
    func reset(){
        games += 1
        randomChoice = Int.random(in: 0..<3)
    }
    func restart(){
        games = 0
        score = 0
        randomChoice = Int.random(in: 0..<3)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
