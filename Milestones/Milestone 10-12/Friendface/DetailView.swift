//
//  DetailView.swift
//  Friendface
//
//  Created by Kashish Jain on 17/08/22.
//

import SwiftUI

struct DetailView: View {
    let user: User
    
    var body: some View {
        VStack{
            VStack(spacing: 10){
                HStack{
                    Spacer()
                    Text(user.isActive ? "Active" : "Inactive")
                        .bold()
                    Circle()
                        .frame(width: 10, height: 10)
                }
                .foregroundColor(user.isActive ? .green : .gray)
                HStack(alignment: .top){
                    Text("Registered: ")
                        .font(.headline.bold())
                    Text(user.registered.formatted(date: .abbreviated, time: .shortened))
                    Spacer()
                }
                HStack(alignment: .top){
                    Text("Email: ")
                        .font(.headline.bold())
                    Text(user.email)
                    Spacer()
                }
                HStack(alignment: .top){
                    Text("Company: ")
                        .font(.headline.bold())
                    Text(user.company)
                    Spacer()
                }
                HStack(alignment: .top){
                    Text("Age: ")
                        .font(.headline.bold())
                    Text(String(user.age))
                    Spacer()
                }
                HStack(alignment: .top){
                    Text("Address: ")
                        .font(.headline.bold())
                    Text(user.address)
                    Spacer()
                }
                HStack(alignment: .top){
                    Text("About: ")
                        .font(.headline.bold())
                    Text(user.about)
                    Spacer()
                }
            }
            .padding()
            Text("Friends")
                .font(.title.bold())
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding(.leading)
            List(user.friends){ friend in
                    Text(friend.name)
            }
            Spacer()
        }
        .navigationTitle(user.name)
        .navigationBarTitleDisplayMode(.inline)
        .ignoresSafeArea(.all, edges: .bottom)
    }
}

let friend1 = Friend(id: UUID(), name: "Friend")
let friend2 = Friend(id: UUID(), name: "Friend")
let friend3 = Friend(id: UUID(), name: "Friend")
let friend4 = Friend(id: UUID(), name: "Friend")

struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView{
            DetailView(user: User(id: UUID(), isActive: true, name: "Name", age: 18, company: "Company", email: "test@user.com", address: "Address", about: "About", registered: Date.now, tags: ["Lorem", "Ipsum"], friends: [friend1, friend2, friend3, friend4]))
        }
    }
}
