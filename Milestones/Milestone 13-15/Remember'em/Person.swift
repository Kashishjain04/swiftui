//
//  Person.swift
//  Remember'em
//
//  Created by Kashish Jain on 02/09/22.
//

import Foundation
import UIKit
import CoreLocation

struct Person: Identifiable, Comparable, Codable {
    let id = UUID()
    let name: String
    let image: Data // image_data
    let latitude: Double
    let longitude: Double
    
    static func <(lhs: Person, rhs: Person) -> Bool {
        lhs.name < rhs.name
    }
}
