//
//  Order.swift
//  CupcakeCorner
//
//  Created by Kashish Jain on 10/08/22.
//

import SwiftUI

struct OrderData: Codable {
    var type = 0
    var quantity = 0
    
    var specialRequestEnabled = false {
        didSet {
            if !specialRequestEnabled {
                extraFrosting = false
                addSprinkles = false
            }
        }
    }
    var extraFrosting = false
    var addSprinkles = false
    
    var name = ""
    var street = ""
    var city = ""
    var pin = ""
}

class Order: ObservableObject {
//    enum CodingKeys: CodingKey {
//        case type, quantity, extraFrosting, addSprinkles, name, street, city, pin
//    }
    
    static let types = ["Vanilla", "Strawberry", "Chocolate", "Rainbow"]
    
    @Published var orderData = OrderData()
    
//    @Published var type = 0
//    @Published var quantity = 0
//
//    @Published var specialRequestEnabled = false {
//        didSet {
//            if !specialRequestEnabled {
//                extraFrosting = false
//                addSprinkles = false
//            }
//        }
//    }
//    @Published var extraFrosting = false
//    @Published var addSprinkles = false
//
//    @Published var name = ""
//    @Published var street = ""
//    @Published var city = ""
//    @Published var pin = ""
    
    var hasValidAddress: Bool {
        if orderData.name.trimmingCharacters(in: .whitespaces).isEmpty || orderData.street.trimmingCharacters(in: .whitespaces).isEmpty || orderData.city.trimmingCharacters(in: .whitespaces).isEmpty || orderData.pin.trimmingCharacters(in: .whitespaces).isEmpty {
            return false
        }
        return true
    }
    
    var cost: Double {
        var cost = Double(orderData.quantity) * 100
        cost += (Double(orderData.type) * 10 * Double(orderData.quantity))
        if orderData.extraFrosting { cost += Double(orderData.quantity) * 15 }
        if orderData.addSprinkles { cost += Double(orderData.quantity) * 5 }
        return cost
    }
    
//    func encode(to encoder: Encoder) throws {
//        var container = encoder.container(keyedBy: CodingKeys.self)
//
//        try container.encode(type, forKey: .type)
//        try container.encode(quantity, forKey: .quantity)
//
//        try container.encode(extraFrosting, forKey: .extraFrosting)
//        try container.encode(addSprinkles, forKey: .addSprinkles)
//
//        try container.encode(name, forKey: .name)
//        try container.encode(street, forKey: .street)
//        try container.encode(city, forKey: .city)
//        try container.encode(pin, forKey: .pin)
//    }
//
//    init() {}
//
//    required init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//
//        type = try container.decode(Int.self, forKey: .type)
//        quantity = try container.decode(Int.self, forKey: .quantity)
//
//        extraFrosting = try container.decode(Bool.self, forKey: .extraFrosting)
//        addSprinkles = try container.decode(Bool.self, forKey: .addSprinkles)
//
//        name = try container.decode(String.self, forKey: .name)
//        street = try container.decode(String.self, forKey: .street)
//        city = try container.decode(String.self, forKey: .city)
//        pin = try container.decode(String.self, forKey: .pin)
//    }
}
