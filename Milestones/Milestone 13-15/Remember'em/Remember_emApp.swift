//
//  Remember_emApp.swift
//  Remember'em
//
//  Created by Kashish Jain on 02/09/22.
//

import SwiftUI

@main
struct Remember_emApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
