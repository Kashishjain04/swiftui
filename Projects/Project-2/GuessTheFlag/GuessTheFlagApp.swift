//
//  GuessTheFlagApp.swift
//  GuessTheFlag
//
//  Created by Kashish Jain on 05/07/22.
//

import SwiftUI

@main
struct GuessTheFlagApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
