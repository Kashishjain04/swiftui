//
//  CheckoutView.swift
//  CupcakeCorner
//
//  Created by Kashish Jain on 10/08/22.
//

import SwiftUI

struct CheckoutView: View {
    @ObservedObject var order = Order()
    @State private var alertTitle = ""
    @State private var confirmationMessage = ""
    @State private var showingConfirmMessage = false
    
    var body: some View {
        ScrollView{
            VStack{
                AsyncImage(url: URL(string: "https://hws.dev/img/cupcakes@3x.jpg"), scale: 3) { image in
                    image
                        .resizable()
                        .scaledToFit()
                } placeholder: {
                    ProgressView()
                }
                .frame(height: 233)
                
                Text("Your total is \(order.cost, format: .currency(code: "INR"))")
                    .font(.title)
                
                Button("Place Order") {
                    Task{
                        await placeOrder()
                    }
                }
                .padding()
            }
        }
        .navigationTitle("Checkout")
        .navigationBarTitleDisplayMode(.inline)
        .alert(alertTitle, isPresented: $showingConfirmMessage){
            Button("OK") {}
        } message: {
            Text(confirmationMessage)
        }
    }
    func placeOrder() async {
        guard let encoded = try? JSONEncoder().encode(order.orderData) else{
            print("Failed to encode order")
            return
        }
        
        let url = URL(string: "https://reqres.in/api/cupcakes")!
        var request = URLRequest(url: url)
        request.setValue("application/json", forHTTPHeaderField: "Content-type")
        request.httpMethod = "POST"
        do {
            let (data, _) = try await URLSession.shared.upload(for: request, from: encoded)
            let decodedOrder = try JSONDecoder().decode(OrderData.self, from: data)
            alertTitle = "Thank you!"
            confirmationMessage = "Your order for \(decodedOrder.quantity)x \(Order.types[decodedOrder.type]) cupcakes is confirmed!"
            showingConfirmMessage = true
        } catch {
            alertTitle = "OOPS!"
            confirmationMessage = "Failed to place order"
            showingConfirmMessage = true
        }
    }
}

struct CheckoutView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView{
            CheckoutView(order: Order())
        }
    }
}
