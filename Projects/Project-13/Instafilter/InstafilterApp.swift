//
//  InstafilterApp.swift
//  Instafilter
//
//  Created by Kashish Jain on 17/08/22.
//

import SwiftUI

@main
struct InstafilterApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
