//
//  ConverterApp.swift
//  Converter
//
//  Created by Kashish Jain on 05/07/22.
//

import SwiftUI

@main
struct ConverterApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
