//
//  TypeStructs.swift
//  Friendface
//
//  Created by Kashish Jain on 17/08/22.
//

import Foundation

struct Friend: Identifiable, Codable {
    let id: UUID
    let name: String
}

struct User: Identifiable, Codable {
    let id: UUID
    let isActive: Bool
    let name: String
    let age: Int
    let company: String
    let email: String
    let address: String
    let about: String
    let registered: Date
    let tags: [String]
    let friends: [Friend]
    
//    init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//        isActive = try container.decode(Bool.self, forKey: .isActive)
//        name = try container.decode(String.self, forKey: .name)
//        age = try container.decode(Int.self, forKey: .age)
//        company = try container.decode(String.self, forKey: .company)
//        email = try container.decode(String.self, forKey: .email)
//        address = try container.decode(String.self, forKey: .address)
//        about = try container.decode(String.self, forKey: .about)
//        registered = try container.decode(ISO8601DateFormatter.self, forKey: .registered)
//        tags = try container.decode([String].self, forKey: .tags)
//        friends = try container.decode([Friend].self, forKey: .friends)
//    }
}
