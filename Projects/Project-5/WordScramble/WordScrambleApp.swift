//
//  WordScrambleApp.swift
//  WordScramble
//
//  Created by Kashish Jain on 15/07/22.
//

import SwiftUI

@main
struct WordScrambleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
