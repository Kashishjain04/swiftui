//
//  WeSplitApp.swift
//  WeSplit
//
//  Created by Kashish Jain on 01/07/22.
//

import SwiftUI

@main
struct WeSplitApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
