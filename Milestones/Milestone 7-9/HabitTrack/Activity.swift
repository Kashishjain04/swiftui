//
//  Activity.swift
//  HabitTrack
//
//  Created by Kashish Jain on 04/08/22.
//

import Foundation

struct Activity: Identifiable, Codable {
    let id = UUID()
    let name: String
    let description: String
    let startTime: Date
    let endTime: Date
}
