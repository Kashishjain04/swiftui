//
//  iExpenseApp.swift
//  iExpense
//
//  Created by Kashish Jain on 27/07/22.
//

import SwiftUI

@main
struct iExpenseApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
