//
//  HotProspectsApp.swift
//  HotProspects
//
//  Created by Kashish Jain on 03/09/22.
//

import SwiftUI

@main
struct HotProspectsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
