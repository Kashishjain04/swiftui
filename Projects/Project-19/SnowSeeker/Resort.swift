//
//  Resort.swift
//  SnowSeeker
//
//  Created by Kashish Jain on 19/09/22.
//

import Foundation

struct Resort: Codable, Identifiable {
    let id: String
    let name: String
    let country: String
    let desc: String
    let imageCredit: String
    let price: Int
    let size: Int
    let snowDepth: Int
    let elevation: Int
    let runs: Int
    let facilities: [String]
    
    static let allResorts: [Resort] = Bundle.main.decode("resorts.json")
    static let example = allResorts[0]
    
    var facilityTypes: [Facility] {
        facilities.map(Facility.init)
    }
    
//    let example = Resort(id: "id", name: "Name", country: "Country", description: "Description", imageCredit: "image", price: 240, size: 40, snowDepth: 40, elevation: 40, runs: 40, facilities: ["Facility-1", "Facility-2"])
}
