//
//  FriendfaceApp.swift
//  Friendface
//
//  Created by Kashish Jain on 17/08/22.
//

import SwiftUI

@main
struct FriendfaceApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
