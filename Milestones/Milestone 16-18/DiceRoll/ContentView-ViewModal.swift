//
//  ContentView-ViewModal.swift
//  DiceRoll
//
//  Created by Kashish Jain on 16/09/22.
//

import Foundation

extension ContentView {
    @MainActor class ViewModal: ObservableObject {
        @Published private(set) var number = 1
        @Published private(set) var timer = Timer.publish(every: 0.1, on: .main, in: .common).autoconnect()
        @Published private var startTime = Date()
        @Published var selectedOption = 6
        @Published private(set) var history = [DiceNumber]()
        
        let options = [4, 6, 8, 10, 12, 20, 100]
        
        private func rollDice() {
            number = Int.random(in: 1...selectedOption)
        }
        private func stopTimer() {
            timer.upstream.connect().cancel()
            updateHistory()
        }
        func startTimer() {
            timer = Timer.publish(every: 0.1, on: .main, in: .common).autoconnect()
            startTime = Date()
        }
        func receiveTimer() {
            rollDice()
            if Date().timeIntervalSince(startTime) >= 1 {
                stopTimer()
            }
        }
        
        // saving JSON
        let savePath = FileManager.documentsDirectory.appendingPathComponent("DiceRoll")
        
        init() {
            do {
                let data = try Data(contentsOf: savePath)
                history = try JSONDecoder().decode([DiceNumber].self, from: data)
            } catch {
                history = []
            }
        }
        
        private func save() {
            do {
                let data = try JSONEncoder().encode(history)
                try data.write(to: savePath, options: [.atomic, .completeFileProtection])
            } catch {
                print("Unable to save history.")
            }
        }
        
        private func updateHistory() {
            let newItem = DiceNumber(id: UUID(), number: number, fromSides: selectedOption)
            history.append(newItem)
            save()
        }
        
    }
}
