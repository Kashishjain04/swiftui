//
//  MapView.swift
//  Remember'em
//
//  Created by Kashish Jain on 02/09/22.
//

import SwiftUI
import MapKit

struct Location: Identifiable {
    let id = UUID()
    let coordinate: CLLocationCoordinate2D
}

struct MapView: View {
    @State private var origLat: Double
    @State private var origLon: Double
    
    @State private var lat: Double
    @State private var lon: Double

    @State private var span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
    
    var markers = [Location]()

    init(latitude: Double, longitude: Double) {
        _lat = State(initialValue: latitude)
        _lon = State(initialValue: longitude)
        
        _origLat = State(initialValue: latitude)
        _origLon = State(initialValue: longitude)
        
        markers.append(Location(coordinate: CLLocationCoordinate2D(latitude: latitude, longitude: longitude)))
    }

    private var region: Binding<MKCoordinateRegion> {
        Binding {
            let centre = CLLocationCoordinate2D(latitude: lat, longitude: lon)
            
            return MKCoordinateRegion(center: centre, span: span)
        } set: { region in
            lat = region.center.latitude
            lon = region.center.longitude
            span = region.span
        }
    }

    var body: some View {
        ZStack{
            Map(coordinateRegion: region, annotationItems: markers){ marker in
                MapPin(coordinate: marker.coordinate)
            }
            VStack{
                Spacer()
                HStack{
                    Spacer()
                    Button{
                        resetMap()
                    }
                    label: {
                        Image(systemName: "location.circle")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 35, height: 35)
                            .clipShape(Circle())
                            .padding(.trailing)
                            .foregroundColor(.primary)
                    }
                }
            }
        }
    }
    func resetMap() {
        withAnimation {
            span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
            lat = origLat
            lon = origLon
        }
    }
}
