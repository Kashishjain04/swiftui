//
//  ContentView-ViewModal.swift
//  Remember'em
//
//  Created by Kashish Jain on 02/09/22.
//

import Foundation
import UIKit

extension ContentView {
    @MainActor class ViewModal: ObservableObject {
        @Published var showAddView = false
        @Published private(set) var people: [Person]
        
        let locationFetcher = LocationFetcher()
        
        func startFetchingLocation() {
            locationFetcher.start()
        }
        
        func stopFetchingLocation() {
            locationFetcher.stop()
        }
        
        func addPerson(name: String, image: UIImage) {
            if let jpegData = image.jpegData(compressionQuality: 0.8) {
                if let location = locationFetcher.lastKnownLocation {
                    let newPerson = Person(name: name, image: jpegData, latitude: location.latitude, longitude: location.longitude)
                    people.append(newPerson)
                    save()
                } else {
                    print("Unable to fetch location data.")
                }
            } else {
                print("Error in encoding image data.")
            }
        }
        
        func deletePerson(at offsets: IndexSet) {
            people.remove(atOffsets: offsets)
            save()
        }
        
        let savePath = FileManager.documentsDirectory.appendingPathComponent("Rememberem")
        
        init() {
            do {
                let data = try Data(contentsOf: savePath)
                people = try JSONDecoder().decode([Person].self, from: data)
            }
            catch {
                people = []
            }
        }
        
        func save() {
            do {
                let data = try JSONEncoder().encode(people)
                try data.write(to: savePath, options: [.atomic, .completeFileProtection])
            }
            catch {
                print("Unable to save people data.")
            }
        }
    }
}
