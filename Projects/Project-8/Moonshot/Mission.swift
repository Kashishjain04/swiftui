//
//  Mission.swift
//  Moonshot
//
//  Created by Kashish Jain on 01/08/22.
//

import Foundation

struct CrewRole: Codable {
    let name: String
    let role: String
}
struct Mission: Codable, Identifiable {
    let id: Int
    let launchDate: String?
    let crew: [CrewRole]
    let description: String
    
    var displayName: String{
        "Apollo \(id)"
    }
    var image: String{
        "apollo\(id)"
    }
}
