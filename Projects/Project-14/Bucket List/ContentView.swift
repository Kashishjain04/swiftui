//
//  ContentView.swift
//  Bucket List
//
//  Created by Kashish Jain on 24/08/22.
//

import SwiftUI
import MapKit

struct ContentView: View {
    @State private var isUnlocked = false
    @StateObject private var viewModal = ViewModal()
    
    var body: some View {
        NavigationView{
            ZStack{
                if viewModal.isUnlocked {
                    Map(coordinateRegion: $viewModal.mapRegion, annotationItems: viewModal.locations) { location in
                        MapAnnotation(coordinate: location.coordinate){
                            VStack{
                                Image(systemName: "star.circle")
                                    .resizable()
                                    .foregroundColor(.red)
                                    .frame(width: 44, height: 44)
                                    .background(.white)
                                    .clipShape(Circle())
                                
                                Text(location.name)
                                    .fixedSize()
                            }
                            .onTapGesture {
                                viewModal.selectedPlace = location
                            }
                        }
                    }
                    .ignoresSafeArea()
                    Circle()
                        .fill(.blue)
                        .opacity(0.3)
                        .frame(width: 32, height: 32)
                    VStack{
                        Spacer()
                        HStack{
                            Spacer()
                            Button{
                                viewModal.addLocation()
                            } label: {
                                Image(systemName: "plus")
                                    .padding()
                                    .background(.black.opacity(0.75))
                                    .foregroundColor(.white)
                                    .font(.title)
                                    .clipShape(Circle())
                                    .padding(.trailing)
                            }
                        }
                    }
                } else {
                    Button("Unlock Places"){
                        viewModal.authenticate()
                    }
                    .padding()
                    .background(.blue)
                    .foregroundColor(.white)
                    .clipShape(Capsule())
                }
            }
            .alert("Authentication Error", isPresented: $viewModal.showAuthenticationError){
                Button("OK"){}
            } message: {
                Text(viewModal.authenticationError)
            }
            .sheet(item: $viewModal.selectedPlace){ place in
                EditView(location: place) {
                    viewModal.updateLocation(location: $0)
                }
            }
            .navigationTitle("Bucket List")
        }

    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ContentView()
        }
    }
}
