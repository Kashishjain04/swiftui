//
//  Astronaut.swift
//  Moonshot
//
//  Created by Kashish Jain on 01/08/22.
//

import Foundation

struct Astronaut: Codable, Identifiable{
    let id: String
    let name: String
    let description: String
}
