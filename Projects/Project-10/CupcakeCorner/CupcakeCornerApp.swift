//
//  CupcakeCornerApp.swift
//  CupcakeCorner
//
//  Created by Kashish Jain on 10/08/22.
//

import SwiftUI

@main
struct CupcakeCornerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
