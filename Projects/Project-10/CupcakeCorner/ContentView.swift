//
//  ContentView.swift
//  CupcakeCorner
//
//  Created by Kashish Jain on 10/08/22.
//

import SwiftUI

struct Response: Codable {
    var results: [Result]
}

struct Result: Codable {
    var trackId: Int
    var trackName: String
    var collectionName: String
}

struct ContentView: View {
    @StateObject var order = Order()
    @State private var results = [Result]()
    
    var body: some View {
        NavigationView {
            Form {
                Section {
                    Picker("Select cake type", selection: $order.orderData.type) {
                        ForEach(Order.types.indices) {
                            Text(Order.types[$0])
                        }
                    }
                    
                    Stepper("Number of cakes: \(order.orderData.quantity)", value: $order.orderData.quantity, in: 1...20)
                }
                Section {
                    Toggle("Any Special Requests?", isOn: $order.orderData.specialRequestEnabled.animation())
                    if order.orderData.specialRequestEnabled {
                        Toggle("Add extra frosting", isOn: $order.orderData.extraFrosting)
                        Toggle("Add extra sprinkles", isOn: $order.orderData.addSprinkles)
                    }
                }
                Section {
                    NavigationLink{
                        AddressView(order: order)
                    } label: {
                        Text("Delivery details")
                    }
                }
            }
            .navigationTitle("Cupcake Corner")
        }
    }
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
