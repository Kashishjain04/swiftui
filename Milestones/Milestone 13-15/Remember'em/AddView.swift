//
//  AddView.swift
//  Remember'em
//
//  Created by Kashish Jain on 02/09/22.
//

import SwiftUI
import Foundation

struct AddView: View {
    @Environment(\.dismiss) var dismiss
    @State private var name = ""
    @State private var showImagePicker = false
    @State private var image = UIImage()
    
    let addPerson: (String, UIImage) -> Void
    
    var body: some View {
        Form {
            Section {
                TextField("Name", text: $name)
            }
            Button{
                showImagePicker = true
            }
            label: {
                HStack {
                    ZStack{
                        Image(systemName: "camera")
                            .resizable()
                            .scaledToFit()
                            .frame(width: 40, height: 40)
                        Image(uiImage: image)
                            .resizable()
                            .scaledToFill()
                            .frame(width: 70, height: 70)
                            .clipShape(Circle())
                    }
                    .padding(4)
                    .background(Color.black.opacity(0.2))
                    .clipShape(Circle())
                    Text("Select Image")
                        .font(.headline)
                }
                .foregroundColor(.primary)
            }
            Section {
                Button("Save"){
                    addPerson(name, image)
                    dismiss()
                }
                .disabled(image.imageAsset == nil || name.isEmpty)
                .font(.headline)
                .frame(maxWidth: .infinity)
            }


        }
        .navigationTitle("Add a Person")
        .sheet(isPresented: $showImagePicker){
            ImagePicker(selectedImage: $image, sourceType: .photoLibrary)
        }
    }
}

struct AddView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView{
            AddView(){ _, _ in
                
            }
        }
    }
}
