//
//  BetterRestApp.swift
//  BetterRest
//
//  Created by Kashish Jain on 13/07/22.
//

import SwiftUI

@main
struct BetterRestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
