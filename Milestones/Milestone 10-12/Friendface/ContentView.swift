//
//  ContentView.swift
//  Friendface
//
//  Created by Kashish Jain on 17/08/22.
//

import SwiftUI

struct ContentView: View {
    @State private var users = [User]()
    var body: some View {
        NavigationView {
            List(users) { user in
                NavigationLink{
                    DetailView(user: user)
                } label: {
                    HStack {
                        VStack(alignment: .leading) {
                            Text(user.name)
                                .font(.headline)
                            Text(user.company)
                                .font(.caption)
                        }
                        Spacer()
                        Circle()
                            .fill(user.isActive ? .green : .gray)
                            .frame(width: 10, height: 10)
                    }
                    .padding(3)
                }
            }
            .task {
                if(users.isEmpty) {
                    await loadData()
                }
            }
            .navigationTitle("Friendface")
        }
    }
    
    
    func loadData() async {
        guard let url = URL(string: "https://www.hackingwithswift.com/samples/friendface.json") else {
            print("Invalid URL")
            return
        }
        do {
            let (data, _) = try await URLSession.shared.data(from: url)
            let decoder = JSONDecoder()
            decoder.dateDecodingStrategy = .iso8601
            if let decodedUsers = try? decoder.decode([User].self, from: data) {
                users = decodedUsers
            }
                
        } catch {
            print("Invalid data")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
