//
//  ActivityView.swift
//  HabitTrack
//
//  Created by Kashish Jain on 04/08/22.
//

import SwiftUI

struct ActivityView: View {
    let activity: Activity
    var body: some View {
        NavigationView{
            VStack(spacing: 10){
                Text(activity.description.isEmpty ? "No Description" : activity.description)
                    .padding(.top, 20)
                HStack{
                    Text("Start: ")
                        .bold()
                    Text(activity.startTime, format: Date.FormatStyle().day().month().year().hour().minute())
                    Spacer()
                }
                .padding(.top, 40)
                .padding(.horizontal)
                HStack{
                    Text("End: ")
                        .bold()
                    Text(activity.endTime, format: Date.FormatStyle().day().month().year().hour().minute())
                    Spacer()
                }
                .padding(.horizontal)
                Spacer()
            }
        }
        .navigationTitle(activity.name)
    }
}

struct ActivityView_Previews: PreviewProvider {
    static let activity = Activity(name: "Test Activity", description: "Loerm ipsum dolor sit amet conseceteur Loerm ipsum dolor sit amet conseceteur Loerm ipsum dolor sit amet conseceteur", startTime: Date(), endTime: Date().addingTimeInterval(12345))
    static var previews: some View {
        ActivityView(activity: activity)
    }
}
