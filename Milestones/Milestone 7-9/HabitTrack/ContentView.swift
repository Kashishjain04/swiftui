//
//  ContentView.swift
//  HabitTrack
//
//  Created by Kashish Jain on 04/08/22.
//

import SwiftUI

struct ContentView: View {
    @StateObject var activities = Activities()
    @State private var showingAddView = false
    
    var body: some View {
        NavigationView{
            List{
                ForEach(activities.activities){ item in
                    NavigationLink{
                        ActivityView(activity: item)
                    } label: {
                        HStack{
                            VStack(alignment: .leading){
                                Text(item.name)
                                    .font(.title3.bold())
                                Spacer()
                                Text(item.description)
                                    .lineLimit(1)
                            }
                            Spacer()
                            VStack{
                                Text(item.startTime...item.endTime)
                            }
                        }
                        .padding(.vertical)
                    }
                }
                .onDelete(perform: removeActivity)
            }
            .navigationTitle("HabitTrack")
            .toolbar{
                Button{
                    showingAddView = true
                } label: {
                    Image(systemName: "plus")
                }
            }
        }
        .sheet(isPresented: $showingAddView){
            AddView(activities: activities)
        }
    }
    func removeActivity(at offsets: IndexSet){
        activities.activities.remove(atOffsets: offsets)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
