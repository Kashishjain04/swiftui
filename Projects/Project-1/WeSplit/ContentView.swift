//
//  ContentView.swift
//  WeSplit
//
//  Created by Kashish Jain on 01/07/22.
//

import SwiftUI

struct ContentView: View {
    @State private var amount = 0.0
    @State private var people = 2
    @State private var tipPercentage = 10
    @FocusState private var inputFocused: Bool
    
    var totalPerPerson: Double {
        let totalAmount = Double(amount) + amount/100.0 * Double(tipPercentage)
        return totalAmount/Double(people)
    }
    
    let percentages = [0, 10, 15, 20, 25]
    var body: some View {
        NavigationView{
            Form{
                Section{
                    TextField("Amount", value: $amount, format: .currency(code: "INR")).keyboardType(.numberPad) .focused($inputFocused)
                    Picker("Number of people", selection: $people){
                        ForEach(2..<100, id: \.self){
                            Text("\($0) People")
                        }
                    }
                }
                Section{
                    Picker("Tip Percentage", selection: $tipPercentage){
                        ForEach(percentages, id: \.self){
                            Text($0, format: .percent)
                        }
                    }.pickerStyle(.segmented)
                }header: {
                    Text("Tip percentage")
                }
                Section{
                    Text(totalPerPerson, format: .currency(code: "INR"))
                        .foregroundColor(tipPercentage == 0 ? .red : .primary)
                } header: {
                    Text("Total Per Person")
                }
            }
            .navigationTitle("WeSplit")
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItemGroup(placement: .keyboard){
                    Spacer()
                    Button("Done"){
                        inputFocused = false
                    }
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
