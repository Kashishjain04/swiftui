//
//  FileManager-DocumentsDirectory.swift
//  Remember'em
//
//  Created by Kashish Jain on 02/09/22.
//

import Foundation

extension FileManager {
    static var documentsDirectory: URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
}
