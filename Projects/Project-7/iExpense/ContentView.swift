//
//  ContentView.swift
//  iExpense
//
//  Created by Kashish Jain on 27/07/22.
//

import SwiftUI

struct ContentView: View {
    @StateObject var expenses = Expenses()
    @State private var showingAddView = false
    @State private var expenseType = "Personal"
    
    var body: some View {
        NavigationView{
            VStack(spacing: 0){
                Picker("Expense Type", selection: $expenseType){
                    ForEach(["Personal", "Business"], id: \.self){
                        Text($0)
                    }
                }.pickerStyle(.segmented).padding()
                List{
                    ForEach(expenses.items.filter{$0.type == expenseType}){ item in
                        HStack{
                            VStack(alignment: .leading){
                                Text(item.name).font(.headline)
                                Text(item.type)
                            }
                            Spacer()
                            Text(item.amount, format: .currency(code: item.currency))
                                .bold()
                        }
                    }
                    .onDelete(perform: removeItems)
                }
                .navigationTitle("iExpense")
                .toolbar{
                    Button{
                        showingAddView = true
                    } label: {
                        Image(systemName: "plus")
                    }
                }
            }
            .sheet(isPresented: $showingAddView){
                AddView(expenses: expenses)
            }
        }
    }
    func removeItems(at offsets: IndexSet){
        expenses.items.remove(atOffsets: offsets)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
