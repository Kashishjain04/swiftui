//
//  Activities.swift
//  HabitTrack
//
//  Created by Kashish Jain on 04/08/22.
//

import Foundation

class Activities: ObservableObject {
    @Published var activities = [Activity](){
        didSet {
            if let encoded = try? JSONEncoder().encode(activities){
                UserDefaults.standard.set(encoded, forKey: "activities")
            }
        }
    }
    
    init(){
        if let savedActivities = UserDefaults.standard.data(forKey: "activities"){
            if let decoded = try? JSONDecoder().decode([Activity].self, from: savedActivities){
                activities = decoded
                return
            }
        }
    }
}
