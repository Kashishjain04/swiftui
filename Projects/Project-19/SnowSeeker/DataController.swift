//
//  DataController.swift
//  SnowSeeker
//
//  Created by Kashish Jain on 21/09/22.
//

import CoreData
import Foundation

class DataController: ObservableObject {
    let container = NSPersistentContainer(name: "Favourites")
    
    init() {
        container.loadPersistentStores { description, error in
            if let error = error {
                print("Coredata failed to load. \(error.localizedDescription)")
            }
            
        }
    }
}
