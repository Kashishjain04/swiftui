//
//  ContentView.swift
//  SnowSeeker
//
//  Created by Kashish Jain on 19/09/22.
//

import SwiftUI

struct User: Identifiable {
    var id: String
}

extension View {
    @ViewBuilder func phoneOnlyStackNavigationView() -> some View {
        if UIDevice.current.userInterfaceIdiom == .phone {
            self.navigationViewStyle(.stack)
        } else {
            self
        }
    }
}

struct ContentView: View {
    @State private var searchText = ""
    @StateObject var favorites = Favorites()
    @Environment(\.managedObjectContext) var moc
    
    @FetchRequest(sortDescriptors: [
        SortDescriptor(\.name)
    ]) var favourites: FetchedResults<Favourites>
    
    let resorts: [Resort] = Bundle.main.decode("resorts.json")
    var filteredResorts: [Resort] {
        if searchText.isEmpty {
            return resorts
        } else {
            return resorts.filter { $0.name.localizedCaseInsensitiveContains(searchText) }
        }
    }
    
    var body: some View {
        NavigationView {
            List(filteredResorts) { resort in
               NavigationLink {
                   ResortView(resort: resort)
               } label: {
                   Image(resort.country)
                       .resizable()
                       .scaledToFill()
                       .frame(width: 40, height: 25)
                       .clipShape(
                           RoundedRectangle(cornerRadius: 5)
                       )
                       .overlay(
                           RoundedRectangle(cornerRadius: 5)
                               .stroke(.black, lineWidth: 1)
                       )

                   VStack(alignment: .leading) {
                       Text(resort.name)
                           .font(.headline)
                       Text("\(resort.runs) runs")
                           .foregroundColor(.secondary)
                   }
                   if favorites.contains(resort) {
                       Spacer()
                       Image(systemName: "heart.fill")
                       .accessibilityLabel("This is a favorite resort")
                           .foregroundColor(.red)
                   }
               }
           }
           .navigationTitle("Resorts")
           .searchable(text: $searchText, prompt: "Search for a resort")
            
            WelcomeView()
        }
        .environmentObject(favorites)
//        .phoneOnlyStackNavigationView()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
