//
//  PersonView.swift
//  Remember'em
//
//  Created by Kashish Jain on 02/09/22.
//

import SwiftUI
import MapKit

struct PersonView: View {
    @Binding var person: Person
    
    
    
    let locationCenter = CLLocationCoordinate2D(latitude: person.latitude, longitude: person.longitude)
    let mapSpan = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
    let markers = [Location(coordinate: locationCenter)]
    
    var body: some View {
        GeometryReader {geo in
            VStack(alignment: .center) {
                Image(uiImage: UIImage(data: person.image ) ?? UIImage())
                    .resizable()
                    .scaledToFit()
                Map(coordinateRegion: MKCoordinateRegion(center: locationCenter, span: mapSpan, annotationItems: markers){ marker in
                    MapPin(coordinate: marker.coordinate)
                })
//                MapView(latitude: person.latitude, longitude: person.longitude)
            }
            .navigationBarTitleDisplayMode(.inline)
            .navigationTitle(person.name)
        }
    }
}

struct PersonView_Previews: PreviewProvider {
    @State static var person = Person(name: "Name", image: Data(), latitude: 53.8862, longitude: 8.6706)
    static var previews: some View {
        NavigationView{
            PersonView(person: person)
        }
    }
}
