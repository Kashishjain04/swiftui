//
//  ContentView.swift
//  BetterRest
//
//  Created by Kashish Jain on 13/07/22.
//

import CoreML
import SwiftUI

extension Binding {
    func onChange(_ handler: @escaping () -> Void) -> Binding<Value> {
        Binding(
            get: { self.wrappedValue },
            set: { newValue in
                self.wrappedValue = newValue
                handler()
            }
        )
    }
}

struct ContentView: View {
    @State private var sleepAmount = 8.0
    @State private var wakeUp = defaultWakeTime
    @State private var coffeeAmount = 1
    @State private var alertTitle = ""
    @State private var alertMsg = ""
    @State private var showAlert = false
    @State private var bedTime = defaultWakeTime - 8*3600
    
    static var defaultWakeTime: Date {
        var components = DateComponents()
        components.hour = 7
        components.minute = 0
        return Calendar.current.date(from: components) ?? Date.now
    }
    
    var body: some View {
        NavigationView{
            VStack{
                Form{
                    Section(header: Text("When do you want to wakeup")){
                        HStack{
                            Spacer()
                            DatePicker("Please enter time", selection: $wakeUp.onChange(calculateBedtime), displayedComponents: .hourAndMinute)
                                .labelsHidden()
                            Spacer()
                        }
                    }
                    Section(header: Text("Desired amount of sleep")){
                        Stepper("\(sleepAmount.formatted()) hours", value: $sleepAmount.onChange(calculateBedtime), in: 4...12, step: 0.25)
                    }
                    Section(header: Text("Daily coffee intake")){
                        Picker("Number of cups", selection: $coffeeAmount.onChange(calculateBedtime)){
                            ForEach(0...10, id: \.self){
                                number in
                                Text("\(number) \(number <= 1 ? "cup" : "cups")")
                            }
                        }.pickerStyle(.wheel).frame(height: 100)
                    }
                    Section(header: Text("Calculated Bedtime")){
                        HStack{
                            Spacer()
                            Text(bedTime.formatted(date: .omitted, time: .shortened))
                                .font(.largeTitle.bold())
                            Spacer()
                        }
                    }
                }
            }
            .onAppear{self.calculateBedtime()}
            .navigationTitle("BetterRest")
            .alert(alertTitle, isPresented: $showAlert){
                Button("OK"){
                    
                }
            } message: {
                Text(alertMsg)
            }
        }
    }
    func calculateBedtime() {
        do{
            let config = MLModelConfiguration()
            let model = try SleepCalculator(configuration: config)
            
            let components = Calendar.current.dateComponents([.hour, .minute], from: wakeUp)
            let hours = (components.hour ?? 0) * 3600
            let minutes = (components.minute ?? 0) * 60
            
            let prediction = try model.prediction(wake: Double(hours+minutes), estimatedSleep: sleepAmount, coffee: Double(coffeeAmount))
            
            bedTime = wakeUp - prediction.actualSleep
//            alertTitle = "Your ideal bedtime is"
//            alertMsg = sleepTime.formatted(date: .omitted, time: .shortened)
        } catch{
            alertTitle = "Error"
            alertMsg = "Sorry! There was a problem calculating your bedtime."
            showAlert = true
        }
    }
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
