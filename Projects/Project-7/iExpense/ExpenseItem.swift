//
//  ExpenseItem.swift
//  iExpense
//
//  Created by Kashish Jain on 28/07/22.
//

import Foundation

// Identifiable -> type can be identified uniquely, a 'id' property must be present

struct ExpenseItem: Identifiable, Codable {
    let id = UUID()
    let name: String
    let type: String
    let amount: Double
    let currency: String
}
