//
//  AddressView.swift
//  CupcakeCorner
//
//  Created by Kashish Jain on 10/08/22.
//

import SwiftUI

struct AddressView: View {
    @ObservedObject var order = Order()
    
    var body: some View {
        Form {
            Section {
                TextField("Name", text: $order.orderData.name)
                TextField("Street", text: $order.orderData.street)
                TextField("City", text: $order.orderData.city)
                TextField("Pin Code", text: $order.orderData.pin)
            }
            Section {
                NavigationLink {
                    CheckoutView(order: order)
                } label: {
                    Text("Proceed to checkout")
                }
            }
            .disabled(!order.hasValidAddress)
        }
        .navigationTitle("Delivery details")
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct AddressView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            AddressView(order: Order())
        }
    }
}
