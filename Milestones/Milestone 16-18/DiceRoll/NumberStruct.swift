//
//  NumberStruct.swift
//  DiceRoll
//
//  Created by Kashish Jain on 16/09/22.
//

import Foundation

struct DiceNumber: Codable, Identifiable {
    var id: UUID
    var number: Int
    var fromSides: Int
    
    static let example = DiceNumber(id: UUID(), number: 12, fromSides: 20)
}
