//
//  ContentView.swift
//  GuessTheFlag
//
//  Created by Kashish Jain on 05/07/22.
//

import SwiftUI

struct FlagView: View{
    var image: String
    var body: some View{
        Image(image)
            .renderingMode(.original)
            .clipShape(Capsule())
            .shadow(radius: 5)
    }
}

struct ContentView: View {
    @State private var countries = ["Estonia", "France", "Germany", "Ireland", "Italy", "Nigeria", "Poland", "Russia", "Spain", "UK", "US"].shuffled()
    @State private var correctAnswer = Int.random(in: 0...2)
    @State private var score: Int = 0
    @State private var trials: Int = 0
    
    @State private var msg = "_"
    
    @State private var tappedNumber = 0
    @State private var rotate = 0.0
    @State private var opacity = 1.0
    
    @State private var tapped = false
    
    let labels = [
        "Estonia": "Flag with three horizontal stripes of equal size. Top stripe blue, middle stripe black, bottom stripe white",
        "France": "Flag with three vertical stripes of equal size. Left stripe blue, middle stripe white, right stripe red",
        "Germany": "Flag with three horizontal stripes of equal size. Top stripe black, middle stripe red, bottom stripe gold",
        "Ireland": "Flag with three vertical stripes of equal size. Left stripe green, middle stripe white, right stripe orange",
        "Italy": "Flag with three vertical stripes of equal size. Left stripe green, middle stripe white, right stripe red",
        "Nigeria": "Flag with three vertical stripes of equal size. Left stripe green, middle stripe white, right stripe green",
        "Poland": "Flag with two horizontal stripes of equal size. Top stripe white, bottom stripe red",
        "Russia": "Flag with three horizontal stripes of equal size. Top stripe white, middle stripe blue, bottom stripe red",
        "Spain": "Flag with three horizontal stripes. Top thin stripe red, middle thick stripe gold with a crest on the left, bottom thin stripe red",
        "UK": "Flag with overlapping red and white crosses, both straight and diagonally, on a blue background",
        "US": "Flag with red and white stripes of equal size, with white stars on a blue background in the top-left corner"
    ]
    
    var body: some View {
        ZStack{
            RadialGradient(stops: [
                .init(color: Color(red: 0.1, green: 0.2, blue: 0.45), location: 0.3),
                .init(color: Color(red: 0.76, green: 0.15, blue: 0.26), location: 0.3),
            ], center: .top, startRadius: 200, endRadius: 700)
            .ignoresSafeArea()
            VStack{
                Spacer()
                Text("Guess The Flag - \(trials+1)/8")
                    .font(.largeTitle.weight(.bold))
                    .foregroundColor(.white)
                VStack(spacing: 30){
                    VStack{
                        Text("Tap the flag of")
                            .foregroundStyle(.secondary)
                            .font(.subheadline.weight(.heavy))
                        Text(countries[correctAnswer])
                            .font(.largeTitle.weight(.semibold))
                    }
                    ForEach(0..<3) {number in
                        Button{
                            if(!tapped){
                                flagTapped(number)
                                withAnimation(.easeInOut(duration: 0.5)){
                                    rotate += 180.0
                                    opacity = 0.25
                                }
                            }
                        } label: {
                            FlagView(image: countries[number])
                                .accessibilityLabel(labels[countries[number], default: "Unknown Flag"])
                        }
                        .rotation3DEffect(Angle(degrees: number == tappedNumber ? rotate : 0), axis: (x: 0, y: 1, z: 0))
                        .opacity(number != tappedNumber ? opacity : 1.0)
                    }
                }
                .frame(maxWidth: .infinity)
                .padding(.vertical, 20)
                .background(.ultraThinMaterial)
                .clipShape(RoundedRectangle(cornerRadius: 20))
                Spacer()
                Spacer()
                Text(msg)
                    .foregroundColor(.white)
                    .font(.title.bold())
                Spacer()
                Text("Score: \(score)")
                    .foregroundColor(.white)
                    .font(.title.bold())
                Spacer()
                Text(trials == 7 ? "Reset" : "Next")
                    .padding(7)
                    .frame(maxWidth: .infinity)
                    .background(.green)
                    .buttonStyle(.borderedProminent)
                    .clipShape(RoundedRectangle(cornerRadius: 10))
                    .onTapGesture(perform: tapped ? (trials == 7 ? reset : askQuestion) : {})
                    .opacity(!tapped ? 0.5 : 1)
            }
            .padding()
        }
    }
    func flagTapped(_ number: Int){
        tappedNumber = number
        tapped = true
        if number == correctAnswer {
            score += 1
            msg = "Correct"
        } else {
            msg = "Wrong! That's the flag of \(countries[number])"
        }
        if trials == 7{
            msg = "Game Over"
        }
    }
    func askQuestion(){
        msg = "_"
        tapped = false
        opacity = 1.0
        rotate = 0.0
        trials += 1
        countries.shuffle()
        correctAnswer = Int.random(in: 0...2)
    }
    func reset(){
        msg = "_"
        tapped = false
        opacity = 1.0
        rotate = 0.0
        trials = 0
        score = 0
        countries.shuffle()
        correctAnswer = Int.random(in: 0...2)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
