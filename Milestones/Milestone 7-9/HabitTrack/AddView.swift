//
//  AddView.swift
//  HabitTrack
//
//  Created by Kashish Jain on 04/08/22.
//

import SwiftUI

struct AddView: View {
    @Environment(\.dismiss) private var dismiss
    @ObservedObject var activities: Activities
    @State private var name = ""
    @State private var description = ""
    @State private var start = Date.now
    @State private var end = Date.now.addingTimeInterval(600)
    var body: some View {
        NavigationView{
            Form{
                TextField("Activity Name", text: $name)
                TextField("Activity Description", text: $description)
                DatePicker("Start Time", selection: $start)
                DatePicker("End Time", selection: $end, in: start...)
            }
            .navigationTitle("Add new activity")
            .toolbar{
                Button("Save"){
                    let newActivity = Activity(name: name, description: description, startTime: start, endTime: end)
                    activities.activities.append(newActivity)
                    dismiss()
                }
                .disabled(name.isEmpty)
            }
        }
    }
}

struct AddView_Previews: PreviewProvider {
    static var previews: some View {
        AddView(activities: Activities())
    }
}
