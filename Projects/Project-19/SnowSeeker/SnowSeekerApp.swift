//
//  SnowSeekerApp.swift
//  SnowSeeker
//
//  Created by Kashish Jain on 19/09/22.
//

import SwiftUI

@main
struct SnowSeekerApp: App {
    @StateObject private var dataController = DataController()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, dataController.container.viewContext)
        }
    }
}
