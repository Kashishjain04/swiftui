//
//  HabitTrackApp.swift
//  HabitTrack
//
//  Created by Kashish Jain on 04/08/22.
//

import SwiftUI

@main
struct HabitTrackApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
