//
//  AddView.swift
//  iExpense
//
//  Created by Kashish Jain on 28/07/22.
//

import SwiftUI

struct AddView: View {
    @Environment(\.dismiss) private var dismiss
    @ObservedObject var expenses: Expenses
    @State private var name = ""
    @State private var type = "Personal"
    @State private var amount = 0.0
    @State private var currency = "INR"
    
    let types = ["Business", "Personal"]
    let currencies = ["INR", "USD", "EUR", "JPY"]
    
    var body: some View {
        NavigationView{
            Form{
                TextField("Name", text: $name)
                Picker("Type", selection: $type){
                    ForEach(types, id: \.self){
                        Text($0)
                    }
                }.pickerStyle(.segmented)
                Picker("Currency", selection: $currency){
                    ForEach(currencies, id: \.self){
                        Text($0)
                    }
                }.pickerStyle(.segmented)
                TextField("Amount", value: $amount, formatter: NumberFormatter())
                    .keyboardType(.decimalPad)
            }
            .navigationTitle("Add new expense")
            .toolbar{
                Button("Save"){
                    let item = ExpenseItem(name: name, type: type, amount: amount, currency: currency)
                    expenses.items.append(item)
                    dismiss()
                }
            }
        }
    }
}

struct AddView_Previews: PreviewProvider {
    static var previews: some View {
        AddView(expenses: Expenses())
    }
}
