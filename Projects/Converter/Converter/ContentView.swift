//
//  ContentView.swift
//  Converter
//
//  Created by Kashish Jain on 05/07/22.
//

import SwiftUI

struct ContentView: View {
    @State private var type = 0
    @State private var input = 0.0
    @State private var ipUnit = 0
    @State private var opUnit = 0
    @FocusState private var inputFocused: Bool
    
    let types = ["Temperature", "Length", "Time", "Volume"]
    let units = [["Celcius", "Fahrenheit"], ["Meters", "Kilometers", "Foot", "Yard", "Miles"], ["Seconds", "Minutes", "Hours"], ["Millilitres", "Litres"]]
    
    func lengthConvert(_ iType: Int, _ oType: Int, _ value: Double) -> Double{
        switch(iType){
        case 0:
            switch(oType){
            case 1: return value/1000.0
            case 2: return value*3.281
            case 3: return value*1.094
            case 4: return value/1609
            default: return value
            }
        case 1:
            switch(oType){
            case 0: return value*1000.0
            case 2: return value*3281
            case 3: return value*1094
            case 4: return value/1.609
            default: return value
            }
        case 2:
            switch(oType){
            case 0: return value/3.281
            case 1: return value/3281
            case 3: return value/3
            case 4: return value/5280
            default: return value
            }
        case 3:
            switch(oType){
            case 0: return value/1.094
            case 1: return value/1094
            case 2: return value*3
            case 4: return value/1760
            default: return value
            }
        case 4:
            switch(oType){
            case 0: return value*1609
            case 1: return value*1.609
            case 2: return value*5280
            case 3: return value*1760
            default: return value
            }
        default: return value
        }
    }
    
    func timeConvert(_ iType: Int, _ oType: Int, _ value: Double) -> Double{
        switch(iType){
        case 0:
            switch(oType){
            case 1: return value/60.0
            case 2: return value/3600.0
            default: return value
            }
        case 1:
            switch(oType){
            case 0: return value*60.0
            case 2: return value/60.0
            default: return value
            }
        case 2:
            switch(oType){
            case 0: return value*3600.0
            case 1: return value*60.0
            default: return value
            }
        default: return value
        }
    }
    
    var output: Double {
        if(ipUnit == opUnit){
            return input
        }
        switch(type){
        case 0:
            switch(ipUnit){
            case 0:
                return input*9.0/5.0 + 32.0
            case 1:
                return (input-32)*5.0/9.0
            default: return input
            }
        case 1: return lengthConvert(ipUnit, opUnit, input)
        case 2: return timeConvert(ipUnit, opUnit, input)
        case 3:
            switch(ipUnit){
            case 0:
                return input/1000.0
            case 1:
                return input*1000.0
            default: return input
            }
        default: return input
        }
    }
    
    var body: some View {
        NavigationView{
            Form{
                Section{
                    Picker("Type of conversion", selection: $type){
                        ForEach(types.indices, id: \.self){
                            Text(types[$0])
                        }
                    }.pickerStyle(.segmented)
                } header: {
                    Text("Select a type of conversion")
                }
                Section{
                    TextField("Input value", value: $input, format: .number).keyboardType(.numberPad).focused($inputFocused)
                    Picker("Input Unit", selection: $ipUnit){
                        ForEach(units[type].indices, id: \.self){
                            Text(units[type][$0])
                        }
                    }.pickerStyle(.segmented)
                    
                } header: {
                    Text("Source")
                }
                Section{
                    Text(output, format: .number)
                    Picker("Input Unit", selection: $opUnit){
                        ForEach(units[type].indices, id: \.self){
                            Text(units[type][$0])
                        }
                    }.pickerStyle(.segmented)
                    
                } header: {
                    Text("Converted")
                }
            }
            .navigationTitle("Unit Converter")
            .navigationBarTitleDisplayMode(.inline)
            .toolbar{
                ToolbarItemGroup(placement: .keyboard){
                    Spacer()
                    Button("Done"){
                        inputFocused = false
                    }
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
