//
//  HistoryView.swift
//  DiceRoll
//
//  Created by Kashish Jain on 16/09/22.
//

import SwiftUI

struct HistoryView: View {
    let history: [DiceNumber]
    var body: some View {
        List(history) { item in
            VStack(alignment: .leading){
                Text("\(item.number)")
                    .font(.title)
                Text("From: \(item.fromSides) sides")
                    .font(.caption)
                    .foregroundColor(.secondary)
            }
        }
        .navigationTitle("History")
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct HistoryView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView{
            HistoryView(history: [DiceNumber](repeating: DiceNumber.example, count: 10))
        }
    }
}
