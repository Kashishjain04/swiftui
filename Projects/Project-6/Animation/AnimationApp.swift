//
//  AnimationApp.swift
//  Animation
//
//  Created by Kashish Jain on 18/07/22.
//

import SwiftUI

@main
struct AnimationApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
