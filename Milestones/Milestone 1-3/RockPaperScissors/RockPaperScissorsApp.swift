//
//  RockPaperScissorsApp.swift
//  RockPaperScissors
//
//  Created by Kashish Jain on 12/07/22.
//

import SwiftUI

@main
struct RockPaperScissorsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
