//
//  ViewsAndModifiersApp.swift
//  ViewsAndModifiers
//
//  Created by Kashish Jain on 11/07/22.
//

import SwiftUI

@main
struct ViewsAndModifiersApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
