//
//  ContentView.swift
//  Remember'em
//
//  Created by Kashish Jain on 02/09/22.
//

import SwiftUI

struct ContentView: View {
    @StateObject private var viewModal = ViewModal()
    
    var body: some View {
        NavigationView{
            ZStack{
                VStack{
                    List{
                        ForEach(viewModal.people) {person in
                            NavigationLink{
                                PersonView(person: person)
                            }
                            label: {
                                HStack {
                                    Image(uiImage: UIImage(data: person.image)!)
                                        .resizable()
                                        .scaledToFill()
                                        .frame(width: 50, height: 50)
                                        .clipShape(Circle())
                                        .padding(4)
                                        .background(.black.opacity(0.2))
                                        .clipShape(Circle())
                                    Text(person.name)
                                        .font(.headline)
                                }
                            }
                        }
                        .onDelete { viewModal.deletePerson(at: $0) }
                    }
                }
                VStack{
                    Spacer()
                    HStack {
                        Spacer()
                        Button{
                            viewModal.showAddView = true
                            viewModal.startFetchingLocation()
                        } label: {
                            Image(systemName: "plus")
                                .padding()
                                .background(.black.opacity(0.75))
                                .foregroundColor(.white)
                                .font(.title)
                                .clipShape(Circle())
                                .padding(.trailing)
                        }
                    }
                }
            }
            .sheet(isPresented: $viewModal.showAddView){
                AddView() { name, image in
                    viewModal.addPerson(name: name, image: image)
                    viewModal.stopFetchingLocation()
                }
            }
            .toolbar {
                EditButton()
            }
            .navigationTitle("Remember'em")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
