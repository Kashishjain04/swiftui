//
//  DiceRollApp.swift
//  DiceRoll
//
//  Created by Kashish Jain on 16/09/22.
//

import SwiftUI

@main
struct DiceRollApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
