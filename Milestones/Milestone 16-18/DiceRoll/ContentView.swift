//
//  ContentView.swift
//  DiceRoll
//
//  Created by Kashish Jain on 16/09/22.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject private var viewModal = ViewModal()
    
    var body: some View {
        NavigationView{
            VStack {
                Form {
                    Picker("Number of sides", selection: $viewModal.selectedOption){
                        ForEach(viewModal.options, id: \.self) { option in
                            Text("\(option) sides")
                                .font(.title)
                                .frame(maxWidth: .infinity)
                        }
                    }
                }
                .frame(maxHeight: 100)
                Spacer()
                VStack(spacing: 50) {
                    Text("\(viewModal.number)")
                        .font(Font.system(size: 80, design: .default).bold())
                        .padding()
                        .frame(width: 200, height: 200)
                        .background(.gray)
                        .cornerRadius(20)
                        .shadow(radius: 10)
                        .navigationTitle("DiceRoll")
                        .onTapGesture{
                            viewModal.startTimer()
                        }
                    
                    Button {
                        viewModal.startTimer()
                    } label: {
                        Label("Roll Dice", systemImage: "play")
                            .font(.title)
                    }
                }
                Spacer()
            }
            .background(Color(UIColor.systemGroupedBackground))
            .toolbar{
                NavigationLink{
                    HistoryView(history: viewModal.history)
                } label: {
                    Label("History", systemImage: "arrowshape.turn.up.backward")
                }
            }
        }
        .onReceive(viewModal.timer){_ in
            viewModal.receiveTimer()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
