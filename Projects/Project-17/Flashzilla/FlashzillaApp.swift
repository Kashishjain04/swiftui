//
//  FlashzillaApp.swift
//  Flashzilla
//
//  Created by Kashish Jain on 13/09/22.
//

import SwiftUI

@main
struct FlashzillaApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
