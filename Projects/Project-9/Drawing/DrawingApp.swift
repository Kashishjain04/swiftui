//
//  DrawingApp.swift
//  Drawing
//
//  Created by Kashish Jain on 02/08/22.
//

import SwiftUI

@main
struct DrawingApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
