//
//  DataController.swift
//  Bookworm
//
//  Created by Kashish Jain on 12/08/22.
//

import CoreData
import Foundation

class DataController: ObservableObject {
    let container = NSPersistentContainer(name: "Bookworm")
    
    init() {
        container.loadPersistentStores { description, error in
            if let error = error {
                print("Coredata failed to load. \(error.localizedDescription)")
            }
            
        }
    }
}
