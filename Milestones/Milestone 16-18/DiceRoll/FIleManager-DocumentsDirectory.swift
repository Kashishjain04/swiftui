//
//  FIleManager-DocumentsDirectory.swift
//  DiceRoll
//
//  Created by Kashish Jain on 16/09/22.
//

import Foundation

extension FileManager {
    static var documentsDirectory: URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
}
