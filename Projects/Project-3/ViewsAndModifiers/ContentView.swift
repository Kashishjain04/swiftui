//
//  ContentView.swift
//  ViewsAndModifiers
//
//  Created by Kashish Jain on 11/07/22.
//

import SwiftUI

struct Watermark: ViewModifier {
    var string: String
    func body(content: Content) -> some View {
        ZStack(alignment: .bottomTrailing){
            content
            Text(string)
                .bold()
                .padding()
                .background(.blue)
                .foregroundColor(.white)
                .clipShape(RoundedRectangle(cornerRadius: 10))
        }
    }
}

struct LargeBlue: ViewModifier{
    func body(content: Content) -> some View{
        content
            .font(.largeTitle)
            .foregroundColor(.blue)
    }
}

extension View{
    func largeBlue() -> some View{
        modifier(LargeBlue())
    }
}

extension View {
    func watermark(with text: String) -> some View {
        modifier(Watermark(string: text))
    }
}

struct Name: View{
    var name: String
    var body: some View{
        Text(name)
            .font(.title)
            .foregroundStyle(.primary)
    }
}

struct ContentView: View {
    @State private var showAlert = false
    var body: some View {
        VStack{
            Spacer()
            Text("Title").largeBlue()
            Spacer()
            ZStack{
                Color.red
                    .watermark(with: "Learn SwiftUI")
                VStack{
                    Spacer()
                    Name(name: "Kashish Jain")
                    Spacer()
                    Name(name: "Kashish Jain")
                    Spacer()
                    Name(name: "Kashish Jain")
                    Spacer()
                    Button("Alert"){
                        showAlert = true
                    }
                }
            }.frame(width: 400, height: 400).clipShape(RoundedRectangle(cornerRadius: 30))
            Spacer()
            Spacer()
        }
        .alert("Alert", isPresented: $showAlert){
            
        } message: {
            Text("WIN !!").font(.largeTitle).bold().foregroundColor(.green)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
