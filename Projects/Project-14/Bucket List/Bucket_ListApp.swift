//
//  Bucket_ListApp.swift
//  Bucket List
//
//  Created by Kashish Jain on 24/08/22.
//

import SwiftUI

@main
struct Bucket_ListApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
